const swal = require('sweetalert2')
const _ = require('lodash')

module.exports = function (args) {
    var template = null
    const customPath = 'templates/allDoneModal.hbs'
    const defaultPath = 'templates/allDoneDefaultModal.hbs'
    try {
        template = require(customPath)
    } catch (err) {
        template = require(defaultPath)
    }

    swal(_.merge(args.swalOptions, {
        html: template(),
        customClass: 'lo-themed allDoneModal'
    }))
    .then(function () {
        const pdf = document.querySelector('.pdf'),
            a = pdf.querySelector('a'),
            i = pdf.querySelector('i')
        
        a.setAttribute(a.getAttribute('_href'))
        
        _.forEach(['animated flip infinite'], function(className){
            i.classList.add(className)
        })
    })
}